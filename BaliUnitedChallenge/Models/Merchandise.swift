//
//  Merchandise.swift
//  BaliUnitedChallenge
//
//  Created by Muhammad Alam Akbar on 1/9/17.
//  Copyright © 2017 Muhammad Alam Akbar. All rights reserved.
//

import Foundation

struct Merchandise {
    var name: String?
    var points: Int?
    var image: String?
    
    init(name: String? = "", points: Int? = 0, image: String?) {
        self.name = name
        self.points = points
        self.image = image
    }
}
