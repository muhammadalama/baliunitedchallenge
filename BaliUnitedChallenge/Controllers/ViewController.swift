//
//  ViewController.swift
//  BaliUnitedChallenge
//
//  Created by Muhammad Alam Akbar on 1/9/17.
//  Copyright © 2017 Muhammad Alam Akbar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var merchandiseTableView: UITableView!
    
    var merchandises: [Merchandise] = [
        Merchandise(name: "Merchandise One Merchandise One Merchandise One", points: 57, image: "img_list_a"),
        Merchandise(name: "Merchandise Two", points: 107, image: "img_list_b"),
        Merchandise(name: "Merchandise Three", points: 207, image: "img_list_c"),
        Merchandise(name: "Merchandise Four", points: 307, image: "img_list_a"),
        Merchandise(name: "Merchandise Five", points: 407, image: "img_list_b"),
        Merchandise(name: "Merchandise Six", points: 507, image: "img_list_c")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        merchandiseTableView.delegate = self
        merchandiseTableView.dataSource = self
        
        merchandiseTableView.register(UINib(nibName: "MerchandiseCell", bundle: nil), forCellReuseIdentifier: "MerchandiseCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return merchandises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MerchandiseCell", for: indexPath) as! MerchandiseCell
        let merchandise = merchandises[indexPath.row] as Merchandise
        cell.merchandise = merchandise
        return cell
    }
    
}
