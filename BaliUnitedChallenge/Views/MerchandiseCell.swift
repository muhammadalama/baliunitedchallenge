//
//  MerchandiseCell.swift
//  BaliUnitedChallenge
//
//  Created by Muhammad Alam Akbar on 1/9/17.
//  Copyright © 2017 Muhammad Alam Akbar. All rights reserved.
//

import UIKit

class MerchandiseCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var redeemButton: UIButton!
    @IBOutlet weak var imageMerchanView: UIImageView!
    
    var merchandise: Merchandise? {
        didSet {
            if let merchandise = merchandise {
                nameLabel.text = merchandise.name
                pointsLabel.text = "\(merchandise.points!) pts"
                imageMerchanView.image = UIImage(named: merchandise.image!)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        //redeemButton.backgroundColor = UIColor(red: 95/255.0, green: 179/255.0, blue: 93/255.0, alpha: 1)
        redeemButton.layer.cornerRadius = 12
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
